CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Requirements
 * Project page
 * Maintainers



INTRODUCTION
------------

This module provides Drupal API integration for the excellent FullCalendar jQuery plugin.
Simple, no-nonsense theme functions and AJAX integrations for FullCalendar events.


RECOMMENDED MODULES
-------------------

 * No extra module is required.


INSTALLATION
------------
1. Install as usual, see
   [Installing Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8)
   for further information.
2. Download requirements
3. Enable FullCalendar API module.
4. Use the provided theme functions to integrate FullCalendar into your project.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core,
but require next js library:

 * Download the version 3.10 [FullCalendar](https://github.com/fullcalendar/fullcalendar/releases/tag/v3.10.0) unpack and place in /libraries.
 * Download Moment.js at https://github.com/moment/moment/releases, unpack and place in /libraries.


PROJECT PAGE
------------

[drupal.org project page](https://www.drupal.org/project/fullcalendar_api)

AUTHORS & MAINTAINERS
---------------------

 * Wes Jones (earthday47) https://www.drupal.org/u/earthday47
 * UsingSession (https://www.drupal.org/u/usingsession)
